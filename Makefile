
ifeq ($(origin CI_REGISTRY_IMAGE),undefined)
CI_REGISTRY_IMAGE := registry.gitlab.com/csp-public/eco-docker-dev
endif

PYTHON_ALPINE35_IMG_VER=$(shell grep "ARG CONT_IMG_VER" python/alpine35/Dockerfile | awk -F'[ =]' '{print $$3}')
PYTHON_ALPINE35_FULL_IMG_VER=$(shell grep "ARG CONT_IMG_VER" python/alpine35-full/Dockerfile | awk -F'[ =]' '{print $$3}')
TOOLS_COMPASS_IMG_VER=$(shell grep "ARG CONT_IMG_VER" tools/compass/Dockerfile | awk -F'[ =]' '{print $$3}')
TOOLS_NODE_IMG_VER=$(shell grep "ARG CONT_IMG_VER" tools/node/Dockerfile | awk -F'[ =]' '{print $$3}')
TOOLS_PYTHON_FORMAT_IMG_VER=$(shell grep "ARG CONT_IMG_VER" tools/python-format/Dockerfile | awk -F'[ =]' '{print $$3}')


build.python.alpine35:
	docker build -t ${CI_REGISTRY_IMAGE}/python:${PYTHON_ALPINE35_IMG_VER} python/alpine35

build.python.alpine35-full:
	docker build -t ${CI_REGISTRY_IMAGE}/python:${PYTHON_ALPINE35_FULL_IMG_VER} python/alpine35-full

build.tools.compass:
	docker build -t ${CI_REGISTRY_IMAGE}/tools/compass:${TOOLS_COMPASS_IMG_VER} tools/compass

build.tools.node:
	docker build -t ${CI_REGISTRY_IMAGE}/tools/node:${TOOLS_NODE_IMG_VER} tools/node

build.tools.python-format:
	docker build -t ${CI_REGISTRY_IMAGE}/tools/python-format:${TOOLS_PYTHON_FORMAT_IMG_VER} tools/python-format
