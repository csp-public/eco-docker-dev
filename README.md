Devs images
===========

[![pipeline status](https://gitlab.com/csp-public/eco-docker-dev/badges/master/pipeline.svg)](https://gitlab.com/csp-public/eco-docker-dev/commits/master)


python-dev:alpine35-*
---------------------

packages:

 * mysql connect
 * Pillow
 * sqlite


python-dev:alpine35-full-*
--------------------------

packages:

 * mysql connect
 * Pillow
 * sqlite
 * scipy, numpy, pandas, pybrain


tools/node:*
------------

packages:

* gulp-cli
* can-compile

tools/compass:*
---------------

packages:

* sass
* compass